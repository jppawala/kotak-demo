package com.example.ktest


import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window
import android.widget.ProgressBar
import android.widget.TextView


class AuthProgressBar {

    var customProgress: AuthProgressBar? = null
    private var mDialog: Dialog? = null

    fun getInstance(): AuthProgressBar? {
        if (customProgress == null) {
            customProgress = AuthProgressBar()
        }
        return customProgress
    }

    fun showProgress(context: Context?, cancelable: Boolean,message:String="Authenticating") {
        mDialog = context?.let { Dialog(it) }
        // no tile for the dialog
        mDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog!!.setContentView(R.layout.custom_progress_dialog)
        var mProgressBar = mDialog!!.findViewById(R.id.progress_loading) as ProgressBar
        //  mProgressBar.getIndeterminateDrawable().setColorFilter(context.getResources()
        // .getColor(R.color.material_blue_gray_500), PorterDuff.Mode.SRC_IN);
        val progressText = mDialog!!.findViewById(R.id.progress_text1) as TextView
        progressText.text = "Authenticating"
        progressText.visibility = View.VISIBLE
        mProgressBar.setVisibility(View.VISIBLE)
        // you can change or add this line according to your need
        mProgressBar.setIndeterminate(true)
        mDialog!!.setCancelable(cancelable)
        mDialog!!.setCanceledOnTouchOutside(cancelable)
        mDialog!!.show()
    }

    fun hideProgress() {
        if (mDialog != null) {
            mDialog!!.dismiss()
            mDialog = null
        }
    }

}