package com.example.ktest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.ktest.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    private val SPLASH_SCREEN_TIME_OUT = 2000
    var progressBar = AuthProgressBar().getInstance();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {
            existingUserLoginBtn.setOnClickListener {
                startActivity(Intent(this@HomeActivity, LoginActivity::class.java))
                finish()
            }
            userLayout.setOnClickListener {
                showLoading()
            }
            skipLoginBtn.setOnClickListener {
                showLoading()
            }


        }


    }

    fun showLoading() {
        progressBar!!.showProgress(this@HomeActivity, true)
        Handler(mainLooper).postDelayed(Runnable {
            progressBar!!.hideProgress()
        }, SPLASH_SCREEN_TIME_OUT.toLong())
    }
}