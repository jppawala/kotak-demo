package com.example.ktest;

import android.content.Context;
import android.util.AttributeSet;


public class KMBTextView extends androidx.appcompat.widget.AppCompatTextView {
    public KMBTextView(Context context) {
        super(context);
    }

    public KMBTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public KMBTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
