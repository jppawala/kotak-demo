package com.example.ktest;

import android.content.Context;
import android.util.AttributeSet;

public class TestEdittext extends androidx.appcompat.widget.AppCompatEditText {
    public TestEdittext(Context context) {
        super(context);
        TestEdittext(context, (AttributeSet) null);
    }

    private void TestEdittext(Context context, AttributeSet attributeSet) {
        String str;
        if (!isInEditMode()) {
            setPaintFlags(getPaintFlags() | 128);
            String string = getResources().getString(R.string.MSF_EDITTEXT_FONT);

        }
    }

    public TestEdittext(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TestEdittext(context, attributeSet);
    }

    public TestEdittext(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TestEdittext(context, attributeSet);
    }


}