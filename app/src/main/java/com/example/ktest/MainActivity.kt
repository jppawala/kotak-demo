package com.example.ktest

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.ktest.databinding.ActivityMainBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.pixplicity.easyprefs.library.Prefs
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    lateinit var mBinding: ActivityMainBinding

    private var mAuth: FirebaseAuth? = null

    var firebaseDatabase: FirebaseDatabase? = null
    var databaseReference: DatabaseReference? = null
    private var courseRVModalArrayList: ArrayList<TransactionModal>? = null


    private val SPLASH_SCREEN_TIME_OUT = 2000
    var progressBar = AuthProgressBar().getInstance();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)


        mBinding.apply {
            contentMain.moreMenuImgLayout.setOnClickListener {
                drawerLayout.openDrawer(Gravity.LEFT)
            }
            contentMain.moreMenuImg.setOnClickListener {
                drawerLayout.openDrawer(Gravity.LEFT)
            }
            loginStatusLayout.setOnClickListener {

            }
            llAddcrn.setOnClickListener {
                showLoading(it)
            }




            nameTxt.setText(Prefs.getString("fullname"))
            crnTxt.setText(Prefs.getString("account"))
            contentMain.accType.setText(Prefs.getString("account"))
            llCmpin.setOnClickListener {
                drawerLayout.close()
                showLoading(it)
            }
            llContactus.setOnClickListener {

                drawerLayout.close()
                showLoading(it)
            }
            llDevice.setOnClickListener {
                drawerLayout.close()
                showLoading(it)
            }
            llFnd.setOnClickListener {
                drawerLayout.close()
                showLoading(it)
            }
            llOneView.setOnClickListener {
                drawerLayout.close()
                showLoading(it)
            }
            llLanguage.setOnClickListener {
                drawerLayout.close()
                showLoading(it)
            }
            llSetting.setOnClickListener {
                drawerLayout.close()
                showLoading(it)
            }


            loginStatusLayout.setOnClickListener {
                Prefs.clear()
                startActivity(Intent(this@MainActivity, HomeActivity::class.java))
                finish()
                finishAffinity()
            }

            contentMain.llApply.setOnClickListener { showLoading(it) }
            contentMain.llCc.setOnClickListener { showLoading(it) }
            contentMain.llInvestment.setOnClickListener { showLoading(it) }
            contentMain.llKaymall.setOnClickListener { showLoading(it) }
            contentMain.llSr.setOnClickListener { showLoading(it) }
            contentMain.llPayment.setOnClickListener { showLoading(it) }

            contentMain.llAo.setOnClickListener {
                startActivity(Intent(this@MainActivity, AccountActivity::class.java))
            }
            contentMain.llAa.setOnClickListener {
                startActivity(Intent(this@MainActivity, AccountActivity::class.java))
            }


            firebaseDatabase = FirebaseDatabase.getInstance()
            mAuth = FirebaseAuth.getInstance()
            courseRVModalArrayList = java.util.ArrayList()
            //on below line we are getting database reference.
            //on below line we are getting database reference.
            databaseReference =
                firebaseDatabase!!.getReference("Transaction").child(Prefs.getString("phone"))


        }
    }

    override fun onResume() {
        super.onResume()
        getCourses()
    }

    fun showLoading(v: View) {
        progressBar!!.showProgress(this@MainActivity, true)
        Handler(mainLooper).postDelayed(Runnable {
            progressBar!!.hideProgress()
        }, SPLASH_SCREEN_TIME_OUT.toLong())
    }

    private fun getCourses() {
        courseRVModalArrayList!!.clear()
        //on below line we are calling add child event listener method to read the data.
        databaseReference!!.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                courseRVModalArrayList!!.add(snapshot.getValue(TransactionModal::class.java)!!)
                //notifying our adapter that data has changed.

                totalCalculation()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                totalCalculation()

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

                totalCalculation()
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }

    fun totalCalculation() {
        var total = 000000000000.0
        for (transactionModal in courseRVModalArrayList!!) {
            total = if (transactionModal.type.equals("Credit")) {
                total + transactionModal.amount.toLong()
            } else {
                total - transactionModal.amount.toLong()
            }
        }
        mBinding.contentMain.showBalanceText.text = "INR " + getFormatedAmount(total)
    }


    private fun getFormatedAmount(amount: Double): String? {
        return NumberFormat.getNumberInstance(Locale.US).format(amount.toBigDecimal())
    }
}