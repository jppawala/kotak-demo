package com.example.ktest;

import android.content.Context;
import android.util.AttributeSet;


public class KMBButton extends androidx.appcompat.widget.AppCompatButton {
    public KMBButton(Context context) {
        super(context);
    }

    public KMBButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public KMBButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
