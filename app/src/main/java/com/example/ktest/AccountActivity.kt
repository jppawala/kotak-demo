package com.example.ktest

import android.R
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ktest.databinding.ActivityAccountBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.pixplicity.easyprefs.library.Prefs
import java.math.BigDecimal
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class AccountActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAccountBinding

    var firebaseDatabase: FirebaseDatabase? = null
    var databaseReference: DatabaseReference? = null
    private var courseRVModalArrayList: ArrayList<TransactionModal>? = null
    private var filterdArrayList: ArrayList<TransactionModal>? = null
    private var courseRVAdapter: TransactionAdapter? = null
    private var mAuth: FirebaseAuth? = null

    private val SPLASH_SCREEN_TIME_OUT = 2000
    var progressBar = AuthProgressBar().getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apply {


            llDrum.txtNumber.setText(Prefs.getString("account"))
            toolbar.topBackBtn.setOnClickListener {
                finish()
            }
        }
        firebaseDatabase = FirebaseDatabase.getInstance()
        mAuth = FirebaseAuth.getInstance()
        courseRVModalArrayList = java.util.ArrayList()
        filterdArrayList = java.util.ArrayList()
        //on below line we are getting database reference.
        //on below line we are getting database reference.
        databaseReference =
            firebaseDatabase!!.getReference("Transaction").child(Prefs.getString("phone"))

        courseRVAdapter = TransactionAdapter(filterdArrayList, this)
        binding.viewTDList.layoutManager = LinearLayoutManager(this)
        binding.viewTDList.adapter = courseRVAdapter


        var list = ArrayList<String>()
        list.add("Last 10 Trans.")
        list.add("View last week")
        list.add("View last month")
        list.add("View last 6 month")
        val spinnerArrayAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            this, R.layout.simple_spinner_item,
            list
        ) //selected item will look like a spinner set from XML

        spinnerArrayAdapter.setDropDownViewResource(R.layout.simple_list_item_single_choice)
        binding.dthOperatorSpinner.setAdapter(spinnerArrayAdapter)
        binding.dthOperatorSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {


                    loadAdapter(position)
                }

            }
        progressBar!!.showProgress(this@AccountActivity, false, "Fetching Account Activity")
        getCourses()
    }

    private fun getCourses() {

        courseRVModalArrayList!!.clear()
        databaseReference!!.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                try {
                    progressBar!!.hideProgress()
                } catch (e: Exception) {

                }

                courseRVModalArrayList!!.add(snapshot.getValue(TransactionModal::class.java)!!)
                courseRVAdapter!!.notifyDataSetChanged()
                totalCalculation()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                //this method is called when new child is added we are notifying our adapter and making progress bar visibility as gone.
                try {
                    progressBar!!.hideProgress()
                } catch (e: Exception) {

                }
                totalCalculation()
                courseRVAdapter!!.notifyDataSetChanged()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                //notifying our adapter when child is removed.
                courseRVAdapter!!.notifyDataSetChanged()
                totalCalculation()
                try {
                    progressBar!!.hideProgress()
                } catch (e: Exception) {

                }
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                //notifying our adapter when child is moved.
                courseRVAdapter!!.notifyDataSetChanged()
                try {
                    progressBar!!.hideProgress()
                } catch (e: Exception) {

                }
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }


    fun totalCalculation() {
        var total = 0.00
        for (transactionModal in courseRVModalArrayList!!) {
            total = if (transactionModal.type.equals("Credit")) {
                total + transactionModal.amount.toLong()
            } else {
                total - transactionModal.amount.toLong()
            }
        }
        binding.availAccbalance.text = getFormatedAmount(total)
    }

    fun loadAdapter(position: Int) {
        filterdArrayList!!.clear()
        if (courseRVModalArrayList!!.isNotEmpty()) {
            Collections.sort(courseRVModalArrayList, object : Comparator<TransactionModal> {
                var f = SimpleDateFormat("EEEE, dd MMMM, yyyy")

                @Override
                override fun compare(lhs: TransactionModal, rhs: TransactionModal): Int {
                    try {
                        return f.parse(lhs.gettDate()).compareTo(f.parse(rhs.gettDate()))
                    } catch (e: ParseException) {
                        throw IllegalArgumentException(e)
                    }
                }
            })
            courseRVModalArrayList!!.reverse()

            if (position == 0) {
                if (courseRVModalArrayList!!.size > 10) {

                    courseRVModalArrayList!!.forEachIndexed { index, transactionModal ->
                        if (index < 10) {
                            filterdArrayList!!.add(transactionModal)

                        }

                    }


                } else {
                    filterdArrayList!!.addAll(courseRVModalArrayList!!)
                }
            } else if (position == 1 || position == 2) {
                var f = SimpleDateFormat("EEEE, dd MMMM, yyyy")
                courseRVModalArrayList!!.forEach {

                    if ((firstDayofMonth().before(f.parse(it.gettDate()))) && (lastDayOfMonth().after(
                            f.parse(
                                it.gettDate()
                            )
                        ))
                        || firstDayofMonth().equals(f.parse(it.gettDate())) || lastDayOfMonth().equals(
                            f.parse(
                                it.gettDate()
                            )
                        )
                    ) {

                        filterdArrayList!!.add(it)
                    }

                }


            } else {
                filterdArrayList!!.addAll(courseRVModalArrayList!!)
            }
            courseRVAdapter!!.notifyDataSetChanged()
        }


    }

    fun firstDayofMonth(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, -1)
        cal[Calendar.DATE] = 1
        val firstDateOfPreviousMonth = cal.time
        return firstDateOfPreviousMonth
    }

    fun lastDayOfMonth(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.MONTH, -1)
        cal[Calendar.DATE] = 1
        val firstDateOfPreviousMonth = cal.time

        cal[Calendar.DATE] = cal.getActualMaximum(Calendar.DATE) // changed calendar to cal
        val lastDateOfPreviousMonth = cal.time
        return lastDateOfPreviousMonth
    }

    private fun getFormatedAmount(amount: Double): String? {
        return NumberFormat.getNumberInstance(Locale.US).format(amount.toBigDecimal())
    }
}