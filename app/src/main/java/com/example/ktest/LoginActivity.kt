package com.example.ktest

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.ktest.databinding.LoginScreenBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.pixplicity.easyprefs.library.Prefs

class LoginActivity : AppCompatActivity() {

    var firebaseDatabase: FirebaseDatabase? = null
    var databaseReference: DatabaseReference? = null
    lateinit var mBinding: LoginScreenBinding
    private var mAuth: FirebaseAuth? = null

    var userModel: UserModel? = null
    private val SPLASH_SCREEN_TIME_OUT = 2000
    var progressBar = AuthProgressBar().getInstance();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = LoginScreenBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        mAuth = FirebaseAuth.getInstance()
        firebaseDatabase = FirebaseDatabase.getInstance()
        mBinding.apply {
            loginBtn.setOnClickListener {
                val phone: String = edtPhone.getText().toString()
                val password: String = edtMpin.getText().toString()
                //on below line validating the text input.
                //on below line validating the text input.
                if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(password)) {
                    Toast.makeText(
                        this@LoginActivity,
                        "Please enter your credentials..",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setOnClickListener
                }
                progressBar!!.showProgress(this@LoginActivity, false)
                mAuth!!.signInWithEmailAndPassword("$phone@gmail.com", password)
                    .addOnCompleteListener(
                        OnCompleteListener<AuthResult?> { task ->
                            //on below line we are checking if the task is succes or not.
                            if (task.isSuccessful) {
                                //on below line we are hiding our progress bar.
                                getLoginDetails(phone)
                            } else {
                                //hiding our progress bar and displaying a toast message.
                                progressBar!!.hideProgress()
                                Toast.makeText(
                                    this@LoginActivity,
                                    "Please enter valid user credentials..",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        })
            }
            btnBottom.addCrn.setOnClickListener {
                showLoading()
            }
            btnBottom.forgotmpin.setOnClickListener {
                showLoading()
            }
            btnBottom.skiplogin.setOnClickListener {
                showLoading()
            }
        }


    }


    fun showLoading() {
        progressBar!!.showProgress(this@LoginActivity, true)
        Handler(mainLooper).postDelayed(Runnable {
            progressBar!!.hideProgress()
        }, SPLASH_SCREEN_TIME_OUT.toLong())
    }

    fun getLoginDetails(phone: String?) {
        databaseReference = firebaseDatabase!!.getReference("Users").child(phone!!)
        databaseReference!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    Log.e("TAG", snapshot.child("account").value.toString())
                    userModel = UserModel()
                    userModel!!.account = snapshot.child("account").value as String?
                    userModel!!.fullname = snapshot.child("fullname").value as String?
                    userModel!!.password = snapshot.child("password").value as String?
                    userModel!!.phone = snapshot.child("phone").value as String?
                    Prefs.putString("account", userModel!!.account)
                    Prefs.putString("fullname", userModel!!.fullname)
                    Prefs.putString("password", userModel!!.password)
                    Prefs.putString("phone", userModel!!.phone)
                    progressBar!!.hideProgress()
                    val i = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(i)
                    finish()
                    finishAffinity()
                }
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }
}