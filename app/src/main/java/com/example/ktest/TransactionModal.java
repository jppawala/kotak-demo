package com.example.ktest;

import android.os.Parcel;
import android.os.Parcelable;

public class TransactionModal implements Parcelable {
    //creating variables for our different fields.
    private String tDate;
    private String description;
    private String amount;
    private String type;
    private String refNumber;


    //creating an empty constructor.
    public TransactionModal() {

    }

    protected TransactionModal(Parcel in) {
        tDate = in.readString();
        description = in.readString();
        amount = in.readString();
        type = in.readString();
        refNumber = in.readString();

    }

    public String gettDate() {
        return tDate;
    }

    public void settDate(String tDate) {
        this.tDate = tDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public static final Creator<TransactionModal> CREATOR = new Creator<TransactionModal>() {
        @Override
        public TransactionModal createFromParcel(Parcel in) {
            return new TransactionModal(in);
        }

        @Override
        public TransactionModal[] newArray(int size) {
            return new TransactionModal[size];
        }
    };


    public TransactionModal(String tDate, String amount, String type, String refNumber, String description) {
        this.tDate = tDate;
        this.amount = amount;
        this.type = type;
        this.refNumber = refNumber;
        this.description = description;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tDate);
        dest.writeString(amount);
        dest.writeString(type);
        dest.writeString(description);

    }
}
