package com.example.ktest

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.pixplicity.easyprefs.library.Prefs


class SplashActivity : AppCompatActivity() {
    private val SPLASH_SCREEN_TIME_OUT = 2000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler(mainLooper).postDelayed(Runnable {
            if (Prefs.contains("phone")) {
                val i = Intent(
                    this@SplashActivity,
                    MainActivity::class.java
                )
                startActivity(i)
                finish()
            } else {
                val i = Intent(
                    this@SplashActivity,
                    HomeActivity::class.java
                )
                startActivity(i)
                finish()
            }

        }, SPLASH_SCREEN_TIME_OUT.toLong())
    }
}