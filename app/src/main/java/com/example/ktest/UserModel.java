package com.example.ktest;

import android.os.Parcel;
import android.os.Parcelable;

public class UserModel implements Parcelable {
    //creating variables for our different fields.
    private String fullname;
    private String account;
    private String password;
    private String phone;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    //creating an empty constructor.
    public UserModel() {

    }

    protected UserModel(Parcel in) {
        fullname = in.readString();
        password = in.readString();
        phone = in.readString();
        account = in.readString();

    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };


    public UserModel(String fullname, String password, String phone, String account) {
        this.fullname = fullname;
        this.password = password;
        this.phone = phone;
        this.account = account;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullname);
        dest.writeString(password);
        dest.writeString(phone);
        dest.writeString(account);
    }
}
