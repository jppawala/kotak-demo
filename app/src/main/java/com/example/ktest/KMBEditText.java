package com.example.ktest;

import android.content.Context;
import android.util.AttributeSet;

public class KMBEditText extends TestEdittext {
    public KMBEditText(Context context) {
        super(context);
    }

    public KMBEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public KMBEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
