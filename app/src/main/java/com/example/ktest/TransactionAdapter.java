package com.example.ktest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    //creating variables for our list, context, interface and position.
    private ArrayList<TransactionModal> courseRVModalArrayList;
    private Context context;
    private CourseClickInterface courseClickInterface;
    int lastPos = -1;

    //creating a constructor.
    public TransactionAdapter(ArrayList<TransactionModal> courseRVModalArrayList, Context context) {
        this.courseRVModalArrayList = courseRVModalArrayList;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //inflating our layout file on below line.
        View view = LayoutInflater.from(context).inflate(R.layout.accountactivity_adapter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //setting data to our recycler view item on below line.
        TransactionModal courseRVModal = courseRVModalArrayList.get(position);

        holder.chqrefNo.setText("Chq/Ref No.:" + courseRVModal.getRefNumber());
        holder.transDesc.setText(courseRVModal.getDescription());
        holder.month.setText(day(getDate(courseRVModal.gettDate())));
        holder.year.setText("'" + year(getDate(courseRVModal.gettDate())));
        holder.monthYear.setText(month(getDate(courseRVModal.gettDate())));

        if (courseRVModal.getType().equalsIgnoreCase("Credit")) {
            holder.balance.setText(courseRVModal.getAmount() + ".00");
            holder.balance.setTextColor(ContextCompat.getColor(context, R.color.po_amount_credited));
        } else {
            holder.balance.setText("-" + courseRVModal.getAmount() + ".00");
            holder.balance.setTextColor(ContextCompat.getColor(context, R.color.po_amount_debited));
        }

    }

    private void setAnimation(View itemView, int position) {
        if (position > lastPos) {
            //on below line we are setting animation.
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            itemView.setAnimation(animation);
            lastPos = position;
        }
    }

    @Override
    public int getItemCount() {
        return courseRVModalArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //creating variable for our image view and text view on below line.
        private ImageView courseIV;
        private TextView year, month, monthYear, transDesc, chqrefNo, balance;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //initializing all our variables on below line.
            year = itemView.findViewById(R.id.year);
            month = itemView.findViewById(R.id.month);
            monthYear = itemView.findViewById(R.id.monthYear);
            transDesc = itemView.findViewById(R.id.transDesc);
            chqrefNo = itemView.findViewById(R.id.chqrefNo);
            balance = itemView.findViewById(R.id.balance);
        }
    }

    //creating a interface for on click
    public interface CourseClickInterface {
        void onCourseClick(int position);
    }

    public Date getDate(String date) {

        SimpleDateFormat format = new SimpleDateFormat("EEEE, dd MMMM, yyyy");
        Date mDate = null;
        try {
            mDate = format.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDate;
    }

    public String day(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd");
        return simpleDateFormat.format(date);
    }

    public String year(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy");
        return simpleDateFormat.format(date);
    }

    public String month(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM");
        return simpleDateFormat.format(date);
    }


}
